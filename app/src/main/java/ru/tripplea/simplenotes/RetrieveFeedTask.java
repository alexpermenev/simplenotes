package ru.tripplea.simplenotes;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;

/**
 * Class for REST API deserialization by URL not in main thread
 */
class RetrieveFeedTask extends AsyncTask<String, Void, JSONObject> {
    protected JSONObject doInBackground(String... urls) {
        try {
            URLConnection urlConnection = new URL(urls[0]).openConnection();
            urlConnection.connect();
            InputStream is = urlConnection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();
            int cp;
            while ((cp = rd.read()) != -1) {
                sb.append((char) cp);
            }
            String jsonText = sb.toString();
            return new JSONObject(jsonText);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
