package ru.tripplea.simplenotes;

import android.app.DialogFragment;
import android.os.Bundle;

import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Objects;

import ru.tripplea.simplenotes.database.model.Note;

/**
 * Fragment of note on main activity
 */
public class NoteFragment extends Fragment {
    private static final String ARG_ID = "id";
    private static final String ARG_TITLE = "title";
    private static final String ARG_BODY = "body";

    private Note note;

    public NoteFragment() {
    }

    /**
     * method for create instance of note's fragment with bundle args
     * @param note note model
     * @return created note's fragment
     */
    public static NoteFragment newInstance(Note note) {
        NoteFragment fragment = new NoteFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_ID, note.getId());
        args.putString(ARG_TITLE, note.getTitle());
        args.putString(ARG_BODY, note.getBody());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        note = new Note();
        if (getArguments() != null) {
            note.setId(getArguments().getInt(ARG_ID));
            note.setTitle(getArguments().getString(ARG_TITLE));
            note.setBody(getArguments().getString(ARG_BODY));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_note, container, false);
        TextView textViewTitle = view.findViewById(R.id.textViewTitle);
        TextView textViewBody = view.findViewById(R.id.textViewBody);
        view.setOnClickListener(view1 -> {
            note.setTitle(textViewTitle.getText().toString());
            note.setBody(textViewBody.getText().toString());
            DialogFragment dialog = CreateNoteDialog.createDialog(note);
            dialog.show(getFragmentManager(), "NoticeDialogFragment");
        });
        textViewTitle.setText(note.getTitle());
        textViewBody.setText(note.getBody());
        return view;
    }
}
